package main

import (
        "fmt"
        "log"
        "net/http"
        "os/exec"
        "strconv"
        "strings"
        "sync"

        "github.com/prometheus/client_golang/prometheus"
        "github.com/prometheus/client_golang/prometheus/promhttp"
)

type Exporter struct {
        metricsMap         map[string]*prometheus.Desc
        scrapeFailures     prometheus.Counter
        syslogNgAlive      *prometheus.Desc
        filesystemMetrics  *prometheus.Desc
        totalProcessedNode *prometheus.Desc
        mutex              sync.Mutex
}

func NewExporter() *Exporter {
        e := &Exporter{
                metricsMap: make(map[string]*prometheus.Desc),
                scrapeFailures: prometheus.NewCounter(prometheus.CounterOpts{
                        Name: "syslog_ng_scrape_failures_total",
                        Help: "Total number of scrape failures.",
                }),
                syslogNgAlive: prometheus.NewDesc(
                        "syslog_ng_alive",
                        "Indicator of whether syslog-ng is alive (1 for alive, 0 for not responding).",
                        nil,
                        nil,
                ),
                filesystemMetrics: prometheus.NewDesc(
                        "filesystem_usage",
                        "Filesystem usage metrics.",
                        []string{"filesystem", "total_size", "used_space", "avail_space", "use_percentage", "volume_name"},
                        nil,
                ),
                totalProcessedNode: prometheus.NewDesc(
                        "syslog_ng_total_processed_node",
                        "Total processed nodes in syslog-ng.",
                        []string{"hostname", "type"},
                        nil,
                ),
        }

        metrics := []string{
                "global.payload_reallocs.processed",
                "destination.d_cobbler.processed",
                "global.scratch_buffers_bytes.queued",
                "global.internal_source.queued",
                "destination.d_kuber.processed",
                "global.scratch_buffers_count.queued",
                "source.s_network.processed",
                "global.internal_source.dropped",
                "global.sdata_updates.processed",
                "global.payload_reallocs.processed",
                "center.received.processed",
                "global.internal_queue_length.processed",
                "center.queued.processed",

        }

        for _, metric := range metrics {
                parts := strings.Split(metric, ".")
                if len(parts) > 1 {
                        name := strings.ReplaceAll(metric, ".", "_")
                        e.metricsMap[metric] = prometheus.NewDesc(
                                "syslog_ng_" + name,
                                fmt.Sprintf("Metric for %s.", metric),
                                []string{"source", "desc", "type"},
                                nil,
                        )
                }
        }

        return e
}

func (e *Exporter) Describe(ch chan<- *prometheus.Desc) {
        for _, m := range e.metricsMap {
                ch <- m
        }
        ch <- e.scrapeFailures.Desc()
        ch <- e.syslogNgAlive
        ch <- e.filesystemMetrics
        ch <- e.totalProcessedNode
}

func (e *Exporter) Collect(ch chan<- prometheus.Metric) {
        var wg sync.WaitGroup

        wg.Add(3)

        cmd := exec.Command("syslog-ng-ctl", "stats")
        _, err := cmd.Output()
        var aliveValue float64
        if err != nil {
                aliveValue = 0
                log.Printf("syslog-ng appears to be down: %v", err)
        } else {
                aliveValue = 1
        }
        ch <- prometheus.MustNewConstMetric(e.syslogNgAlive, prometheus.GaugeValue, aliveValue)

        go e.collectSyslogMetrics(ch, &wg)
        go e.collectFilesystemMetrics(ch, &wg)
        go e.collectTotalProcessedNodes(ch, &wg)

        wg.Wait()
}

func (e *Exporter) collectSyslogMetrics(ch chan<- prometheus.Metric, wg *sync.WaitGroup) {
        defer wg.Done()
        for metricPath := range e.metricsMap {
                e.mutex.Lock()
                cmd := exec.Command("syslog-ng-ctl", "query", "get", metricPath)
                output, err := cmd.Output()
                if err != nil {
                        e.scrapeFailures.Inc()
                        log.Printf("Error executing command for metric %s: %v", metricPath, err)
                        e.mutex.Unlock()
                        continue
                }
                e.mutex.Unlock()

                outputParts := strings.SplitN(strings.TrimSpace(string(output)), "=", 2)
                if len(outputParts) != 2 {
                        log.Printf("Unexpected output format for %s: %s", metricPath, string(output))
                        continue
                }

                metricValue, err := strconv.ParseFloat(outputParts[1], 64)
                if err != nil {
                        log.Printf("Error parsing metric value for %s: %v", metricPath, err)
                        continue
                }

                labels := strings.Split(metricPath, ".")
                if len(labels) >= 3 {
                        desc, exists := e.metricsMap[metricPath]
                        if !exists {
                                log.Printf("Descriptor for metric %s does not exist", metricPath)
                                continue
                        }

                        ch <- prometheus.MustNewConstMetric(desc, prometheus.GaugeValue, metricValue, labels...)
                }
        }
}

func (e *Exporter) collectFilesystemMetrics(ch chan<- prometheus.Metric, wg *sync.WaitGroup) {
        defer wg.Done()
        cmd := exec.Command("df", "-h")
        output, err := cmd.Output()
        if err != nil {
                log.Printf("Error executing df command: %v", err)
                return
        }

        lines := strings.Split(string(output), "\n")
        for _, line := range lines {
                if strings.Contains(line, "/logs") {
                        fields := strings.Fields(line)
                        if len(fields) == 6 {
                                filesystem := fields[0]
                                total_size := fields[1]
                                used_space := fields[2]
                                avail_space := fields[3]
                                use_precentage := fields[4]
                                volume_name := fields[5]

                                ch <- prometheus.MustNewConstMetric(e.filesystemMetrics, prometheus.GaugeValue, 1, filesystem, total_size, used_space, avail_space, use_precentage, volume_name)
                        }
                }
        }
}

func (e *Exporter) collectTotalProcessedNodes(ch chan<- prometheus.Metric, wg *sync.WaitGroup) {
        defer wg.Done()
        cmd := exec.Command("syslog-ng-ctl", "query", "get", "src.host.*.processed")
        output, err := cmd.Output()
        if err != nil {
                log.Printf("Error executing command for total processed node: %v", err)
                return
        }

        lines := strings.Split(strings.TrimSpace(string(output)), "\n")
        for _, line := range lines {
                fields := strings.Split(line, "=")
                if len(fields) == 2 {
                        parts := strings.Split(fields[0], ".")
                        if len(parts) >= 4 && parts[0] == "src" && parts[1] == "host" {
                                hostname := strings.Join(parts[2:len(parts)-1], ".")
                                metricValue, err := strconv.ParseFloat(fields[1], 64)
                                if err != nil {
                                        log.Printf("Error parsing metric value for total processed node: %v", err)
                                        continue
                                }
                                ch <- prometheus.MustNewConstMetric(e.totalProcessedNode, prometheus.GaugeValue, metricValue, hostname, "processed")
                        }
                }
        }
}
func main() {
        exporter := NewExporter()
        prometheus.MustRegister(exporter)
        http.Handle("/metrics", promhttp.Handler())

        fmt.Println("Serving metrics on :8080/metrics")
        log.Fatal(http.ListenAndServe(":8080", nil))
}