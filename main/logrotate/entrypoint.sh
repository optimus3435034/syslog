#!/bin/bash

# Get the sleep time from the argument (default to 3600 seconds if no argument is provided)
SLEEP_TIME=${1:-3600}
VERSION="1.0.2"
RELEASE="26.03.2024"

echo -en "\n-= Welcome to Logrotate by Optimus Team =-"
echo -en "\nVersion: $VERSION Released: $RELEASE Interval: $SLEEP_TIME seconds\n"

# Start syslog-ng
/usr/sbin/syslog-ng -F &

# Run logrotate in a loop
while true; do
    /usr/sbin/logrotate /etc/logrotate.conf
    sleep $SLEEP_TIME
done &

# Wait for syslog-ng to finish (if it ever does)
wait
