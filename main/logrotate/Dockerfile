#ARG LAUNCHPAD_BUILD_ARCH
#LABEL org.opencontainers.image.version=22.04

FROM ubuntu:jammy

ARG PKG_TYPE=stable

# Install necessary packages
RUN apt-get update -qq \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y \
        wget \
        ca-certificates \
        gnupg \
        libdbd-mysql \
        libdbd-pgsql \
        libdbd-sqlite3 \
        syslog-ng \
        libjemalloc2 \
        logrotate \
        vim \
        tree \
    && rm -rf /var/lib/apt/lists/*

# Add syslog-ng repository and key
RUN wget -qO - https://ose-repo.syslog-ng.com/apt/syslog-ng-ose-pub.asc | apt-key add - \
    && echo "deb https://ose-repo.syslog-ng.com/apt/ ${PKG_TYPE} ubuntu-jammy main" | tee /etc/apt/sources.list.d/syslog-ng-ose.list \
    && apt-get update -qq \
    && apt-get install -y syslog-ng \
    && rm -rf /var/lib/apt/lists/*

# Install Go
RUN apt-get update -qq \
    && apt-get install -y \
        wget \
        tar \
    && wget https://go.dev/dl/go1.21.0.linux-amd64.tar.gz \
    && tar -C /usr/local -xzf go1.21.0.linux-amd64.tar.gz \
    && rm go1.21.0.linux-amd64.tar.gz \
    && export GOROOT=/usr/local/go \
    && export GOPATH=$HOME/go \
    && export PATH=$GOPATH/bin:$GOROOT/bin:$PATH \
    && echo 'export GOROOT=/usr/local/go' >> ~/.profile \
    && echo 'export GOPATH=$HOME/go' >> ~/.profile \
    && echo 'export PATH=$GOPATH/bin:$GOROOT/bin:$PATH' >> ~/.profile

# Copy syslog-ng configuration
#COPY syslog-ng.conf /etc/syslog-ng/syslog-ng.conf
COPY settings.conf /etc/settings.conf
COPY rotate-logs.sh /rotate-logs.sh

RUN chmod +x /rotate-logs.sh
# Set entrypoint
CMD ["/rotate-logs.sh"]