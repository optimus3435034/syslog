apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "syslog-ng.fullname" . }}
  labels:
    {{- include "syslog-ng.labels" . | nindent 4 }}
spec:
{{- if not .Values.autoscaling.enabled }}
  replicas: {{ .Values.replicaCount }}
{{- end }}
  selector:
    matchLabels:
      {{- include "syslog-ng.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      annotations:
        checksum/config: {{ tpl (toYaml .Values.config) . | sha256sum }}
    {{- with .Values.podAnnotations }}
        {{- toYaml . | nindent 8 }}
    {{- end }}
      labels:
        {{- include "syslog-ng.selectorLabels" . | nindent 8 }}
    spec:
      {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      shareProcessNamespace: true
      containers:
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          ports:
          - name: {{ .Values.ports.tcp.name }}
            containerPort: {{ .Values.ports.tcp.containerPort }}
            protocol: {{ .Values.ports.tcp.protocol }}
          - name: {{ .Values.ports.udp.name }}
            containerPort: {{ .Values.ports.udp.containerPort }}
            protocol: {{ .Values.ports.udp.protocol }}
          - name: {{ .Values.ports.go.name }}
            containerPort: {{ .Values.ports.go.targetPort }}
            protocol: {{ .Values.ports.go.protocol }}
          volumeMounts:
          - mountPath: {{ .Values.volumeMounts.mountPath }}
            name: {{ .Values.volumeMounts.name }}
            subPath: {{ .Values.volumeMounts.subPath }}
{{- if .Values.storage.enable }}
          - mountPath: {{ .Values.storage.mountPath }}
            name: {{ .Values.storage.name }}
          - mountPath: /logs_local 
            name: syslog-logs
            name: logrotate-config
          - mountPath: /etc/logrotate.conf
            name: logrotate-config
            subPath: logrotate.conf
{{- end }}
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
        - name: {{ .Values.sideCar.name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: {{ .Values.sideCar.image }}
          volumeMounts:
            - name: {{ .Values.sideCar.volumeMounts.config.name }}
              mountPath: {{ .Values.sideCar.volumeMounts.config.mountPath }}
              subPath: {{ .Values.sideCar.volumeMounts.config.subPath }}
            - name: {{ .Values.sideCar.volumeMounts.logs.name }}
              mountPath: {{ .Values.sideCar.volumeMounts.logs.mountPath }}
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
      volumes:
        - name: config
          configMap:
            name: {{ include "syslog-ng.fullname" . }}
        - name: {{ .Values.volumes.name }}
          configMap:
            name: {{ .Values.volumes.configMap.name }}
            items:
              - key: {{ .Values.volumes.configMap.items.key }}
                path: {{ .Values.volumes.configMap.items.path }}
        - name: logrotate-config
          configMap:
            name: {{ .Values.logrotate.configMap.name }}
            items:
              - key: {{ .Values.logrotate.configMap.key }}
                path: logrotate.conf
{{- if .Values.storage.enable }}
        - name: {{ .Values.storage.name }}
          persistentVolumeClaim:
            claimName: {{ include "syslog-ng.fullname" . }}-pvc
{{- end }}
        - name: syslog-logs  # Name of the hostPath volume for syslog-ng logs
          hostPath:
            path: /tmp/shared_logs  # The path on the host
            type: DirectoryOrCreate  # This will create the directory if it doesn't exist
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}